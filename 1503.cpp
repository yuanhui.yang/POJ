// 1503. Integer Inquiry
// http://poj.org/problem?id=1503

/*
Description

One of the first users of BIT's new supercomputer was Chip Diller. He extended his exploration of powers of 3 to go from 0 to 333 and he explored taking various sums of those numbers. 
``This supercomputer is great,'' remarked Chip. ``I only wish Timothy were here to see these results.'' (Chip moved to a new apartment, once one became available on the third floor of the Lemon Sky apartments on Third Street.) 
Input

The input will consist of at most 100 lines of text, each of which contains a single VeryLongInteger. Each VeryLongInteger will be 100 or fewer characters in length, and will only contain digits (no VeryLongInteger will be negative). 

The final input line will contain a single zero on a line by itself. 
Output

Your program should output the sum of the VeryLongIntegers given in the input.
Sample Input

123456789012345678901234567890
123456789012345678901234567890
123456789012345678901234567890
0
Sample Output

370370367037037036703703703670
Source

East Central North America 1996
*/

#include <iostream>
#include <string>
using namespace std;

string add(string s, string t) {
	size_t m = s.size(), n = t.size(), carry = 0, base = 10, i = m - 1, j = n - 1, k = max(m, n);
	string result(k + 1, ' ');
	while (carry or i != string::npos or j != string::npos) {
		size_t x = i != string::npos ? s[i--] - '0' : 0;
		size_t y = j != string::npos ? t[j--] - '0' : 0;
		size_t z = x + y + carry;
		carry = z / base;
		result[k--] = '0' + z % base;
	}
	return result.substr(k + 1);
}

int main(void) {
	string s, t;

	s = "0";
	while (cin >> t and t != "0") {
		s = add(s, t);
	}
	cout << s;

	return 0;
}