# Peking University Judge Online for ACMICPC

| # | Title | Solution |
| --- | --- | --- |
| [1000](http://poj.org/problem?id=1000) | [A+B Problem](http://poj.org/problem?id=1000) | [C++](https://github.com/yuanhui-yang/POJ/blob/master/1000.cpp) |
| [1151](http://poj.org/problem?id=1151) | [Atlantis](http://poj.org/problem?id=1151) | [C++](https://github.com/yuanhui-yang/POJ/blob/master/1151.cpp) |
| [1503](http://poj.org/problem?id=1503) | [Integer Inquiry](http://poj.org/problem?id=1503) | [C++](https://github.com/yuanhui-yang/POJ/blob/master/1503.cpp) |