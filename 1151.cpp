// 1151. Atlantis
// http://poj.org/problem?id=1151

/*
Description

There are several ancient Greek texts that contain descriptions of the fabled island Atlantis. Some of these texts even include maps of parts of the island. But unfortunately, these maps describe different regions of Atlantis. Your friend Bill has to know the total area for which maps exist. You (unwisely) volunteered to write a program that calculates this quantity.
Input

The input consists of several test cases. Each test case starts with a line containing a single integer n (1 <= n <= 100) of available maps. The n following lines describe one map each. Each of these lines contains four numbers x1;y1;x2;y2 (0 <= x1 < x2 <= 100000;0 <= y1 < y2 <= 100000), not necessarily integers. The values (x1; y1) and (x2;y2) are the coordinates of the top-left resp. bottom-right corner of the mapped area. 
The input file is terminated by a line containing a single 0. Don't process it.
Output

For each test case, your program should output one section. The first line of each section must be "Test case #k", where k is the number of the test case (starting with 1). The second one must be "Total explored area: a", where a is the total explored area (i.e. the area of the union of all rectangles in this test case), printed exact to two digits to the right of the decimal point. 
Output a blank line after each test case.
Sample Input

2
10 10 20 20
15 15 25 25.5
0
Sample Output

Test case #1
Total explored area: 180.00 
Source

Mid-Central European Regional Contest 2000
*/

#include <iostream>
#include <set>
#include <vector>
#include <algorithm>
using namespace std;

class Solution {
public:
	double atlantis(const vector<vector<double> > & A) {
		vector<Line> X;
		for (size_t i = 0; i < A.size(); ++i) {
			X.push_back(Line(A[i][0], A[i][1], A[i][3], -1));
			X.push_back(Line(A[i][2], A[i][1], A[i][3], 1));
		}
		sort(X.begin(), X.end());
		multiset<pair<double, double> > Y;
		double result = 0;
		for (size_t i = 0; i + 1 < X.size(); ++i) {
			pair<double, double> e({X[i].y1, X[i].y2});
			if (X[i].z < 0) {
				Y.insert(e);
			}
			else {
				Y.erase(Y.find(e));
			}
			double dx = X[i + 1].x - X[i].x, dy = g(Y);
			result += dx * dy;
		}
		return result;
	}
private:
	struct Line {
		double x, y1, y2;
		int z;
		Line(double x, double y1, double y2, int z) {
			this->x = x;
			this->y1 = y1;
			this->y2 = y2;
			this->z = z;
		}
		bool operator< (const Line & other) const {
			if (this->x == other.x) {
				if (this->z == other.z) {
					if (this->z < 0) {
						return this->y1 < other.y1;
					}
					return this->y1 > other.y1;
				}
				return this->z < other.z;
			}
			return this->x < other.x;
		}
	};
	double g(const multiset<pair<double, double> > & Y) {
		double result = 0;
		for (multiset<pair<double, double> >::iterator it = Y.begin(); it != Y.end(); ++it) {
			double a = (*it).first, b = (*it).second;
			multiset<pair<double, double> >::iterator jt = it;
			++jt;
			while (jt != Y.end() and (*jt).first <= b) {
				b = max(b, (*jt).second);
				++it;
				jt = it;
				++jt;
			}
			result += b - a;
		}
		return result;
	}
};

int main(void) {
	Solution solution;
	int N, k = 0;
	while (cin >> N and N > 0) {
		vector<vector<double> > A;
		for (int i = 0; i < N; ++i) {
			vector<double> e(4);
			cin >> e[0] >> e[1] >> e[2] >> e[3];
			A.push_back(e);
		}
		cout.precision(2);
		cout << fixed;
		cout << "Test case #" << ++k << '\n';
		cout << "Total explored area: " << solution.atlantis(A) << "\n\n";
	}
	return 0;
}